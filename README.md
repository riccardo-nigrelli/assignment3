# Assignment 3
Viene realizzata un'applicazione che implementa CRUD (Create, Read, Update, Delete) attraverso l'utilizzo del framework Hibernate che implementa JPA (Java Persistence API).

## Utilizzo
1. Scaricare la seguente Repository
2. Importare il progetto nell'IDE che si preferisce

Se l'IDE scelto è *Eclipse*:

3. Andare su `File → Import...`
4. Scegliere `Gradle → Existing Gradle Project` e successivamente cliccare su `Next`
5. Selezione la cartella contenente il progetto e cliccare su `Next`
6. Nella sezione delle *Import Option* lasciare tutto di default e cicclare su `Next`
7. Una volta che gradle avrà finito premere su `Finish`
    
se è *IntelliJ IDEA*:

6. Nella finestra di avvio, scegliere `Import Project`
7. Selezionare la cartella contenente il progetto e cliccare su `Open`
8. Controllare che siano selezionati `Import project from external model` e `Gradle`; se si premere su `Next` 
9. Aggiungere (se non presente) la spunta su `Use auto-import` e cliccare su `Finish`

A questo punto, indipendentemente dal tipo di IDE scelto, rencandosi nel package `src/test/java/entity` si trovano i test di unità delle entità. Per eseguirli, selezionarli singolarmente e facendo click con il tasto destro scegliere `Run As → JUnit Test` se si usa Eclipse altrimenti, `Run 'TestClassName'`
    