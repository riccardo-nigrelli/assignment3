package dao;

import entity.Image;
import util.Dao;

import java.util.List;


public class ImageDAO extends Dao<Image> {

    @Override
    public void create(Image entity) {
        getEntityManager().persist(entity);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Image> research(String field, String value) {
        final String sql = "select i from Image i where i." + field + " like '%" + value + "%'";
        return getEntityManager().createQuery(sql).getResultList();
    }

    @Override
    public void update(Image entity) {
        getEntityManager().merge(entity);
    }

    @Override
    public void delete(Long primaryKey) {

        Image image = getItemByPrimaryKey(primaryKey);

        image.getTopics().forEach(topic -> {
            topic.removeImage(image);
        });

        getEntityManager().remove(image);
    }

    @Override
    public Image getItemByPrimaryKey(Long primaryKey) {
        return getEntityManager().find(Image.class, primaryKey);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Image> getAllItem() {
        return getEntityManager().createQuery("select i from Image i").getResultList();
    }
}
