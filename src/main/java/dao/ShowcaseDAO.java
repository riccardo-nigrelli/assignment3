package dao;

import entity.Showcase;
import util.Dao;

import java.util.List;

public class ShowcaseDAO extends Dao<Showcase> {

    @Override
    public void create(Showcase entity) {
        getEntityManager().persist(entity);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Showcase> research(String field, String value) {
        final String sql = "select t from Showcase t where t." + field + " like '%" + value + "%'";
        return getEntityManager().createQuery(sql).getResultList();
    }

    @Override
    public void update(Showcase entity) {
        getEntityManager().merge(entity);
    }

    @Override
    public void delete(Long primaryKey) {
        getEntityManager().remove(getItemByPrimaryKey(primaryKey));
    }

    @Override
    public Showcase getItemByPrimaryKey(Long primaryKey) {
        return getEntityManager().find(Showcase.class, primaryKey);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Showcase> getAllItem() {
        return getEntityManager().createQuery("select s from Showcase s").getResultList();
    }
}
