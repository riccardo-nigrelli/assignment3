package dao;

import entity.Topic;
import util.Dao;

import java.util.List;

public class TopicDAO extends Dao<Topic> {

    @Override
    public void create(Topic entity) {
        getEntityManager().persist(entity);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Topic> research(String field, String value) {
        final String sql = "select t from Topic t where t." + field + " like '%" + value + "%'";
        return getEntityManager().createQuery(sql).getResultList();
    }

    @Override
    public void update(Topic entity) {
        getEntityManager().merge(entity);
    }

    @Override
    public void delete(Long primaryKey) {

        Topic topic = getItemByPrimaryKey(primaryKey);

        topic.getImages().forEach(image -> {
            image.removeTopic(topic);
        });

        getEntityManager().remove(topic);
    }

    @Override
    public Topic getItemByPrimaryKey(Long primaryKey) {
        return getEntityManager().find(Topic.class, primaryKey);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Topic> getAllItem() {
        return getEntityManager().createQuery("select t from Topic t").getResultList();
    }
}
