package dao;

import entity.User;
import util.Dao;

import javax.persistence.EntityManager;
import java.util.List;

public class UserDAO extends Dao<User> {

    @Override
    public void create(User entity) {
        getEntityManager().persist(entity);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<User> research(String field, String value) {
        final String sql = "select u from User u where u." + field + " like '%" + value + "%'";
        return getEntityManager().createQuery(sql).getResultList();
    }

    @Override
    public void update(User entity) {
        getEntityManager().merge(entity);
    }

    @Override
    public void delete(Long primaryKey) {

        EntityManager entityManager = getEntityManager();

        entityManager.createQuery("update Image i set owner.id = null where owner.id = :id")
                .setParameter("id", primaryKey).executeUpdate();

        entityManager.createQuery("update Showcase s set owner.id = null where owner.id = :id")
                .setParameter("id", primaryKey).executeUpdate();

        User user = getItemByPrimaryKey(primaryKey);

        user.getFollowers().forEach(u -> {
            user.removeFollower(u);
        });

        getEntityManager().remove(user);
    }

    @Override
    public List<User> getAllItem() {
        return getEntityManager().createQuery("select u from User u").getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public User getItemByPrimaryKey(Long primaryKey) {
        return getEntityManager().find(User.class, primaryKey);
    }
}
