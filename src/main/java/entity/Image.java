package entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "image")
public class Image implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_sequence")
    @SequenceGenerator(name = "id_sequence", sequenceName = "ID_SEQUENCE")
    private Long id;

    @Column(length = 150, nullable = false)
    private String description;

    @ManyToOne(fetch = FetchType.EAGER)
    private User owner;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinTable(
            name = "typology",
            joinColumns = {@JoinColumn(name = "image_id")},
            inverseJoinColumns = {@JoinColumn(name = "topic_id")}
    )
    private Set<Topic> topics = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "imageSet")
    private Set<Showcase> showcaseSet = new HashSet<>();

    public Image(){

    }

    public Image(String descrizione, User owner, Topic topic){
        this.description = descrizione;
        setOwner(owner);
        addTopic(topic);
    }

    public Image(String descrizione, User owner, List<Topic> topic){
        this.description = descrizione;
        setOwner(owner);

        topic.forEach(t -> {
            addTopic(t);
        });

    }

    public Long getId(){
        return id;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public String getDescription(){
        return description;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public User getOwner(){
        return owner;
    }

    public Set<Topic> getTopics(){
        return topics;
    }

    public void addTopic(Topic topic){
        topics.add(topic);
    }

    public void removeTopic(Topic topic){
        topics.remove(topic);
    }

    public Set<Showcase> getShowcaseSet(){
        return showcaseSet;
    }

    public void addImageToShowcase(Showcase showcase){
        this.showcaseSet.add(showcase);
    }

    public void removeImageToShowcase(Showcase showcase){
        showcaseSet.remove(showcase);
    }

    @Override
    public String toString(){
        return "Image: { id: " + id + ", owner: " + owner.getId() + ", description: " + description + " }";
    }

    @Override
    public boolean equals(Object object){

        if(this == object) return true;
        if(object == null) return false;
        if(getClass() != object.getClass()) return false;

        Image image = (Image) object;

        if(!id.equals(image.id)) return false;
        if(!description.equals(image.description)) return false;
        if(!owner.equals(image.owner)) return false;

        return true;
    }

}
