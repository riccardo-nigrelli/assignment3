package entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "showcase")
public class Showcase implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_sequence")
    @SequenceGenerator(name = "id_sequence", sequenceName = "ID_SEQUENCE")
    private Long id;

    @Column(length = 50)
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    private User owner;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "showcase_include_images",
            joinColumns = {@JoinColumn(name = "showcase_id")},
            inverseJoinColumns = {@JoinColumn(name = "image_id")}
    )
    private Set<Image> imageSet = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "followedShowcase")
    private Set<User> userSet = new HashSet<>();

    public Showcase(){

    }

    public Showcase(String name, User owner){
        this.name = name;

        setOwner(owner);
        owner.addShowcase(this);
    }

    public Long getId(){
        return id;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public User getOwner(){
        return owner;
    }

    public void setOwner(User owner){
        this.owner = owner;
    }

    public Set<Image> getImageSet(){
        return imageSet;
    }

    public void addImageToShowcase(Image image){
        this.imageSet.add(image);
    }

    public void removeImageToShowcase(Image image){
        imageSet.remove(image);
    }

    public Set<User> getUserSet(){
        return userSet;
    }

    public void addShowcaseFollower(User user){
        this.userSet.add(user);
    }

    public void removeShowcaseFollower(User user){
        userSet.remove(user);
    }

    @Override
    public String toString(){
        return "Showcase: { id: " + id + ", name: " + name + " }";
    }

    @Override
    public boolean equals(Object object){

        if(this == object) return true;
        if(object == null) return false;
        if(getClass() != object.getClass()) return false;

        Showcase showcase = (Showcase) object;

        if(!id.equals(showcase.id)) return false;
        if(!name.equals(showcase.name)) return false;

        return true;
    }
}
