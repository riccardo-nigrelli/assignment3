package entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "topic")
public class Topic implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_sequence")
    @SequenceGenerator(name = "id_sequence", sequenceName = "ID_SEQUENCE")
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "topics")
    private Set<Image> images = new HashSet<>();

    public Topic(){}

    public Topic(String name){
        this.name = name;
    }

    public Long getId(){
        return id;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public Set<Image> getImages(){
        return images;
    }

    public void removeImage(Image image){
        images.add(image);
    }

    @Override
    public String toString(){
        return "Topic: { id: " + id + ", name: " + name + " }";
    }

    @Override
    public boolean equals(Object object){

        if(this == object) return true;
        if(object == null) return false;
        if(getClass() != object.getClass()) return false;

        Topic topic = (Topic) object;

        if(!id.equals(topic.id)) return false;
        if(!name.equals(topic.name)) return false;

        return true;
    }
}
