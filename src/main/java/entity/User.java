package entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_sequence")
    @SequenceGenerator(name = "id_sequence", sequenceName = "ID_SEQUENCE")
    private Long id;

    @Column(length = 50, nullable = false)
    private String email;

    @Column(length = 30, nullable = false)
    private String password;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "owner")
    private Set<Image> imageSet = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(name = "user_follow_user")
    private Set<User> followers = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE, mappedBy = "followers")
    private Set<User> followings = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "owner")
    private Set<Showcase> showcaseSet = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "user_follow_showcase"
    )
    private Set<Showcase> followedShowcase = new HashSet<>();

    public User(){

    }

    public User(String email, String password){
        this.email = email;
        this.password = password;
    }

    public Long getId(){
        return id;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public String getEmail(){
        return email;
    }

    public void setPassword(String password){
        this.password = password;
    }

    public String getPassword(){
        return password;
    }

    public Set<Image> getImageSet(){
        return imageSet;
    }

    public void addImage(Image image){
        this.imageSet.add(image);
        image.setOwner(this);
    }

    public void removeImage(Image image){
        imageSet.remove(image);
    }

    public Set<User> getFollowers(){
        return followers;
    }

    public void addFollower(User follower){
        this.followers.add(follower);
        follower.addFollowing(this);
    }

    public void removeFollower(User follower){
        followers.remove(follower);
        follower.removeFollowing(this);
    }

    public Set<User> getFollowings(){
        return followings;
    }

    public void addFollowing(User following){
        this.followings.add(following);
    }

    public void removeFollowing(User following){
        followings.remove(following);
    }

    public Set<Showcase> getShowcaseSet(){
        return showcaseSet;
    }

    public void addShowcase(Showcase showcase){
        this.showcaseSet.add(showcase);
    }

    public void removeShowcase(Showcase showcase){
        showcaseSet.remove(showcase);
    }

    public Set<Showcase> getFollowedShowcase(){
        return followedShowcase;
    }

    public void addFollowerShowcase(Showcase showcase){
        this.followedShowcase.add(showcase);
    }

    public void removeFollowerShowcase(Showcase showcase){
        followedShowcase.remove(showcase);
    }

    @Override
    public String toString(){
        return "User: { id: " + id + ", email: " + email + ", password: " + password + " }";
    }

    @Override
    public boolean equals(Object object){

        if(this == object) return true;
        if(object == null) return false;
        if(getClass() != object.getClass()) return false;

        User user = (User) object;

        if(! id.equals(user.id)) return false;
        if(! email.equals(user.email)) return false;
        if(! password.equals(user.password)) return false;

        return true;
    }
}
