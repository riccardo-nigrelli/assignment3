package service;

import dao.ImageDAO;
import entity.Image;
import util.Service;

import java.util.List;

public class ImageService implements Service<Image> {

    private ImageDAO imageDAO;

    public ImageService(){
        imageDAO = new ImageDAO();
    }

    @Override
    public void create(Image entity) {
        imageDAO.beginTransaction();
        imageDAO.create(entity);
        imageDAO.commitTransaction();
    }

    @Override
    public List<Image> research(String field, String value) {
        imageDAO.beginTransaction();
        List<Image> list = imageDAO.research(field, value);
        imageDAO.commitTransaction();

        return list;
    }

    @Override
    public void update(Image entity) {
        imageDAO.beginTransaction();
        imageDAO.update(entity);
        imageDAO.commitTransaction();
    }

    @Override
    public void delete(Long primaryKey) {
        imageDAO.beginTransaction();
        imageDAO.delete(primaryKey);
        imageDAO.commitTransaction();
    }

    @Override
    public Image getItemByPrimaryKey(Long primaryKey) {
        imageDAO.beginTransaction();
        Image image = imageDAO.getItemByPrimaryKey(primaryKey);
        imageDAO.commitTransaction();

        return image;
    }

    @Override
    public List<Image> getAllItem() {
        imageDAO.beginTransaction();
        List<Image> list = imageDAO.getAllItem();
        imageDAO.commitTransaction();

        return list;
    }
}
