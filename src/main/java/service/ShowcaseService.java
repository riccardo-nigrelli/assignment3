package service;

import dao.ShowcaseDAO;
import entity.Showcase;
import util.Service;

import java.util.List;

public class ShowcaseService implements Service<Showcase> {

    private ShowcaseDAO showcaseDAO;

    public ShowcaseService(){
        showcaseDAO = new ShowcaseDAO();
    }

    @Override
    public void create(Showcase entity) {
        showcaseDAO.beginTransaction();
        showcaseDAO.create(entity);
        showcaseDAO.commitTransaction();
    }

    @Override
    public List<Showcase> research(String field, String value) {
        showcaseDAO.beginTransaction();
        List<Showcase> list = showcaseDAO.research(field, value);
        showcaseDAO.commitTransaction();

        return list;
    }

    @Override
    public void update(Showcase entity) {
        showcaseDAO.beginTransaction();
        showcaseDAO.update(entity);
        showcaseDAO.commitTransaction();
    }

    @Override
    public void delete(Long primaryKey) {
        showcaseDAO.beginTransaction();
        showcaseDAO.delete(primaryKey);
        showcaseDAO.commitTransaction();
    }

    @Override
    public Showcase getItemByPrimaryKey(Long primaryKey) {
        showcaseDAO.beginTransaction();
        Showcase showcase = showcaseDAO.getItemByPrimaryKey(primaryKey);
        showcaseDAO.commitTransaction();

        return showcase;
    }

    @Override
    public List<Showcase> getAllItem() {
        showcaseDAO.beginTransaction();
        List<Showcase> list = showcaseDAO.getAllItem();
        showcaseDAO.commitTransaction();

        return list;
    }
}
