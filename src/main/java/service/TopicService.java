package service;

import dao.TopicDAO;
import entity.Topic;
import util.Service;

import java.util.List;

public class TopicService implements Service<Topic> {

    private TopicDAO topicDAO;

    public TopicService(){
        topicDAO = new TopicDAO();
    }

    @Override
    public void create(Topic entity) {
        topicDAO.beginTransaction();
        topicDAO.create(entity);
        topicDAO.commitTransaction();
    }

    @Override
    public List<Topic> research(String field, String value) {
        topicDAO.beginTransaction();
        List<Topic> list = topicDAO.research(field, value);
        topicDAO.commitTransaction();

        return list;
    }

    @Override
    public void update(Topic entity) {
        topicDAO.beginTransaction();
        topicDAO.update(entity);
        topicDAO.commitTransaction();
    }

    @Override
    public void delete(Long primaryKey) {
        topicDAO.beginTransaction();
        topicDAO.delete(primaryKey);
        topicDAO.commitTransaction();
    }

    @Override
    public Topic getItemByPrimaryKey(Long primaryKey) {
        topicDAO.beginTransaction();
        Topic topic = topicDAO.getItemByPrimaryKey(primaryKey);
        topicDAO.commitTransaction();

        return topic;
    }

    @Override
    public List<Topic> getAllItem() {
        topicDAO.beginTransaction();
        List<Topic> list = topicDAO.getAllItem();
        topicDAO.commitTransaction();

        return list;
    }
}
