package service;

import dao.UserDAO;
import entity.User;
import util.Service;

import java.util.List;

public class UserService implements Service<User> {

    private UserDAO userDAO;

    public UserService(){
        userDAO = new UserDAO();
    }

    @Override
    public void create(User entity) {
        userDAO.beginTransaction();
        userDAO.create(entity);
        userDAO.commitTransaction();
    }

    @Override
    public List<User> research(String field, String value) {
        userDAO.beginTransaction();
        List<User> list = userDAO.research(field, value);
        userDAO.commitTransaction();

        return list;
    }

    @Override
    public void update(User entity) {
        userDAO.beginTransaction();
        userDAO.update(entity);
        userDAO.commitTransaction();
    }

    @Override
    public void delete(Long primaryKey) {
        userDAO.beginTransaction();
        userDAO.delete(primaryKey);
        userDAO.commitTransaction();
    }

    @Override
    public User getItemByPrimaryKey(Long primaryKey) {
        userDAO.beginTransaction();
        User user = userDAO.getItemByPrimaryKey(primaryKey);
        userDAO.commitTransaction();

        return user;
    }

    @Override
    public List<User> getAllItem() {
        userDAO.beginTransaction();
        List<User> list = userDAO.getAllItem();
        userDAO.commitTransaction();

        return list;
    }

}
