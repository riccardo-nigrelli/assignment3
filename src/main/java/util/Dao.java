package util;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class Dao<T> {

    public abstract void create(T entity);
    public abstract List<T> research(String field, String value);
    public abstract void update(T entity);
    public abstract void delete(Long primaryKey);
    public abstract T getItemByPrimaryKey(Long primaryKey);
    public abstract List<T> getAllItem();


    public Dao(){}

    public EntityManager getEntityManager() {
        return EntityManagerUtil.getEntityManager();
    }

    public void beginTransaction() {
        getEntityManager().getTransaction().begin();
    }

    public void commitTransaction() {
        getEntityManager().flush();
        getEntityManager().getTransaction().commit();
    }

}
