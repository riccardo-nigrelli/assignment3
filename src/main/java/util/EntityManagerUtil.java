package util;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class EntityManagerUtil {

    private static EntityManager entityManager = null;

    private EntityManagerUtil(){

    }

    public static EntityManager getEntityManager() {
        if(entityManager == null)
            entityManager =  Persistence.createEntityManagerFactory("assignment3").createEntityManager();

        return entityManager;
    }

}
