package util;

import java.util.List;

public interface Service<T> {

    void create(T entity);
    List<T> research(String field, String value);
    void update(T entity);
    void delete(Long primaryKey);
    T getItemByPrimaryKey(Long primaryKey);
    List<T> getAllItem();

}
