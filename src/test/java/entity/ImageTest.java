package entity;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import service.ImageService;
import service.TopicService;
import service.UserService;
import util.Service;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.List;

import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ImageTest {

    private Service<Image> imageService = new ImageService();
    private Service<User> userService = new UserService();
    private Service<Topic> topicService = new TopicService();

    private Image image, image1, image2;
    private User user;
    private Topic topic;
    private List<Image> imageList;
    private List<User> userList;

    @BeforeClass
    public static void setUp(){
        drop();
    }

    @Test
    public void test1Create() {

        user = new User("user@example.com", "user");
        userService.create(user);

        topic = new Topic("Mathematics");
        topicService.create(topic);

        image = new Image("Riemann Hypothesis", user, topic);

        image1 = new Image();
        image1.setDescription("Schrodinger Equation");
        image1.setOwner(user);
        image1.addTopic(topic);

        assertTrue(image.getDescription().equals("Riemann Hypothesis"));
        assertTrue(image.getOwner().equals(user));

        assertEquals("Schrodinger Equation", image1.getDescription());
        assertTrue(image1.getOwner().equals(user));

        imageService.create(image);
        imageService.create(image1);
    }

    @Test
    public void test2ResearchAllItem() {

        imageList = imageService.getAllItem();
        assertEquals(2, imageList.size());

        userList = userService.getAllItem();
        user = userList.get(0);

        image = imageList.get(0);
        assertEquals("Riemann Hypothesis", image.getDescription());
        assertTrue(user.equals(image.getOwner()));

        image1 = imageList.get(1);
        assertEquals("Schrodinger Equation", image1.getDescription());
        assertTrue(user.equals(image1.getOwner()));
    }

    @Test
    public void test3ResearchByPrimaryKey() {

        imageList = imageService.getAllItem();
        image = imageList.get(0);

        image2 = imageService.getItemByPrimaryKey(image.getId());
        assertTrue(image.equals(image2));
    }

    @Test
    public void test4ResearchByField() {

        imageList = imageService.research("description", "n");
        assertEquals(2, imageList.size());
    }

    @Test
    public void test5Update() {

        imageList = imageService.getAllItem();
        image = imageList.get(0);

        image.setDescription("Stark Effect");

        imageService.update(image);

        imageList = imageService.getAllItem();
        image1 = imageList.get(0);

        assertTrue(image1.getDescription().equals("Stark Effect"));
    }

    @Test
    public void test6DeleteByUser() {

        userList = userService.getAllItem();
        user = userList.get(0);

        userService.delete(user.getId());

        imageList = imageService.getAllItem();
        assertTrue(imageList.size() == 2);
    }

    @Test
    public void test7Delete() {

        imageList = imageService.getAllItem();
        image = imageList.get(0);
        imageService.delete(image.getId());

        imageList = imageService.getAllItem();
        assertTrue(imageList.size() == 1);
    }

    @AfterClass
    public static void clearDatabase(){
        drop();
    }

    private static void drop(){
        final EntityManager entityManager = Persistence.createEntityManagerFactory("assignment3").createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.createNativeQuery("DELETE FROM user WHERE 1").executeUpdate();
        entityManager.createNativeQuery("DELETE FROM topic WHERE 1").executeUpdate();
        entityManager.createNativeQuery("DELETE FROM image WHERE 1").executeUpdate();
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}