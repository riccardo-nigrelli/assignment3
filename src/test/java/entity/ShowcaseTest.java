package entity;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import service.ImageService;
import service.ShowcaseService;
import service.TopicService;
import service.UserService;
import util.Service;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ShowcaseTest {

    private Service<Showcase> showcaseService = new ShowcaseService();
    private Service<User> userService = new UserService();
    private Service<Image> imageService = new ImageService();
    private Service<Topic> topicService = new TopicService();

    private User user, user1;
    private Image image, image1, image2;
    private Topic topic, topic1, topic2;
    private List<Topic> topicList;
    private Showcase showcase, showcase1, showcase2;
    private List<Showcase> showcaseList;

    @BeforeClass
    public static void setUp(){
        drop();
    }

    @Test
    @SuppressWarnings("serial")
    public void test1Create(){

        user = new User("pippo@example.com", "pippo");
        user1 = new User("pluto@example", "pluto");

        userService.create(user);
        userService.create(user1);

        topic = new Topic("Mathematics");
        topic1 = new Topic("Physics");
        topic2 = new Topic("Quantum Mechanics");

        topicList = new ArrayList<Topic>(){{
            add(topic1);
            add(topic2);
        }};

        topicService.create(topic);
        topicService.create(topic1);
        topicService.create(topic2);

        image = new Image("Quantum Teleportation", user, topicList);
        image1 = new Image("Stark Effect", user, topic2);
        image2 = new Image("Riemann Hypothesis", user1, topic);

        imageService.create(image);
        imageService.create(image1);
        imageService.create(image2);

        showcase = new Showcase();
        showcase.setName("Quantum Physics");
        showcase.setOwner(user);

        showcase1 = new Showcase("Complex Analysis", user1);

        assertEquals("Quantum Physics", showcase.getName());
        assertTrue(showcase.getOwner().equals(user));
        assertEquals("Complex Analysis", showcase1.getName());
        assertTrue(showcase1.getOwner().equals(user1));

        showcase.addImageToShowcase(image);
        showcase.addImageToShowcase(image1);

        showcaseService.create(showcase);
        showcaseService.create(showcase1);
    }

    @Test
    public void test2ResearchAllItem(){

        showcaseList = showcaseService.getAllItem();
        assertEquals(2, showcaseList.size());

        showcase = showcaseList.get(0);
        user = userService.getAllItem().get(0);
        assertEquals("Quantum Physics", showcase.getName());
        assertTrue(user.equals(showcase.getOwner()));
    }

    @Test
    public void test3ResearchByPrimaryKey(){

        showcaseList = showcaseService.getAllItem();
        showcase = showcaseList.get(0);

        showcase2 = showcaseService.getItemByPrimaryKey(showcase.getId());
        assertTrue(showcase2.equals(showcase));
    }

    @Test
    public void test4ResearchByField(){

        showcaseList = showcaseService.research("name", "Complex Analysis");
        assertEquals(1, showcaseList.size());

        showcase = showcaseService.getAllItem().get(1);

        showcase1 = showcaseList.get(0);
        assertTrue(showcase1.equals(showcase));
    }

    @Test
    public void test5InsertImageInShowcase(){

        showcaseList = showcaseService.getAllItem();
        showcase1 = showcaseList.get(1);

        List<Image> imageList = imageService.getAllItem();
        image2 = imageList.get(2);

        showcase1.addImageToShowcase(image2);
        showcaseService.update(showcase1);

        showcaseList = showcaseService.getAllItem();
        showcase1 = showcaseList.get(1);

        assertTrue(showcase1.getImageSet().contains(image2));
    }

    @Test
    public void test6UserFollowShowcase(){

        showcaseList = showcaseService.getAllItem();
        showcase1 = showcaseList.get(1);

        user = userService.getAllItem().get(0);
        user.addFollowerShowcase(showcase1);
        userService.update(user);

        user = userService.getAllItem().get(0);
        showcase1 = showcaseService.getAllItem().get(1);

        System.out.println(user.getFollowedShowcase());
        System.out.println(showcase1);

        assertTrue(user.getFollowedShowcase().contains(showcase1));
    }

    @Test
    public void test7DeleteByUser(){

        user = userService.getAllItem().get(0);
        userService.delete(user.getId());

        assertTrue(showcaseService.research("owner.id", String.valueOf(user.getId())).isEmpty());
    }

    @Test
    public void test8Delete(){

        showcaseList = showcaseService.getAllItem();

        showcaseList.forEach(showcase -> {
            showcaseService.delete(showcase.getId());
        });

        assertTrue(showcaseService.getAllItem().isEmpty());
    }

    @AfterClass
    public static void clearDatabase(){
        drop();
    }

    private static void drop(){
        final EntityManager entityManager = Persistence.createEntityManagerFactory("assignment3").createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.createNativeQuery("DELETE FROM user WHERE 1").executeUpdate();
        entityManager.createNativeQuery("DELETE FROM topic WHERE 1").executeUpdate();
        entityManager.createNativeQuery("DELETE FROM image WHERE 1").executeUpdate();
        entityManager.createNativeQuery("DELETE FROM showcase WHERE 1").executeUpdate();
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}