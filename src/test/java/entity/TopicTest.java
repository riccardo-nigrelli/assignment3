package entity;

import org.junit.*;
import org.junit.runners.MethodSorters;
import service.ImageService;
import service.TopicService;
import service.UserService;
import util.Service;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.List;

import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TopicTest {

    private Service<Topic> topicService = new TopicService();
    private Service<Image> imageService = new ImageService();
    private Service<User> userService = new UserService();

    private Topic topic, topic1, topic2;
    private List<Topic> topicList;
    private User user;
    private Image image, image1;

    @BeforeClass
    public static void setUp(){
        drop();
    }

    @Test
    public void test1Create(){
        topic = new Topic();
        topic.setName("Quantum Computing");

        topic1 = new Topic();
        topic1.setName("Mathematics");

        assertEquals("Quantum Computing", topic.getName());
        assertEquals("Mathematics", topic1.getName());

        topicService.create(topic);
        topicService.create(topic1);

        user = new User("pippo@example.com", "pippo");
        userService.create(user);

        image = new Image("Riemann Hypothesis", user, topic);
        image1 = new Image("Schrodinger Equation", user, topic1);

        imageService.create(image);
        imageService.create(image1);
    }

    @Test
    public void test2ResearchAllItem(){

        topicList = topicService.getAllItem();
        assertEquals(2, topicList.size());

        topic = topicList.get(0);
        assertEquals("Quantum Computing", topic.getName());

        topic1 = topicList.get(1);
        assertEquals("Mathematics", topic1.getName());
    }

    @Test
    public void test3ResearchByPrimaryKey(){

        topicList = topicService.getAllItem();
        topic = topicList.get(0);

        topic2 = topicService.getItemByPrimaryKey(topic.getId());
        assertTrue(topic.equals(topic2));

    }

    @Test
    public void test4ResearchByField(){

        topicList = topicService.research("name", "Mathematics");
        assertEquals(1, topicList.size());

        topic2 = topicList.get(0);

        topicList = topicService.getAllItem();
        topic1 = topicList.get(1);

        assertTrue(topic1.equals(topic2));

        topicList = topicService.research("name", "m");
        assertEquals(2, topicList.size());
    }

    @Test
    public void test5Update(){

        topicList = topicService.getAllItem();
        topic = topicList.get(0);

        topic.setName("Quantum Mechanics");

        topicService.update(topic);

        topicList = topicService.getAllItem();
        topic2 = topicList.get(0);

        assertTrue(topic2.getName().equals("Quantum Mechanics"));
    }

    @Test
    public void test6Delete(){
        List<Topic> topics = topicService.getAllItem();
        topic = topics.get(0);
        topic1 = topics.get(1);

        topicService.delete(topic.getId());
        topicService.delete(topic1.getId());

        assertTrue(topicService.getAllItem().isEmpty());
    }

    @AfterClass
    public static void clearDatabase(){
        drop();
    }

    private static void drop(){
        final EntityManager entityManager = Persistence.createEntityManagerFactory("assignment3").createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.createNativeQuery("DELETE FROM user WHERE 1").executeUpdate();
        entityManager.createNativeQuery("DELETE FROM topic WHERE 1").executeUpdate();
        entityManager.createNativeQuery("DELETE FROM image WHERE 1").executeUpdate();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}