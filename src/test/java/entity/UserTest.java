package entity;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import service.UserService;
import util.Service;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.List;

import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserTest {

    private User user, user1, user2;
    private Service<User> userService = new UserService();
    private List<User> userList;

    @BeforeClass
    public static void setUp(){
        drop();
    }

    @Test
    public void test1Create(){

        user = new User();
        user.setEmail("user@example.com");
        user.setPassword("user");

        user1 = new User("user1@example.com", "user1");

        assertEquals("user@example.com", user.getEmail());
        assertEquals("user1@example.com", user1.getEmail());
        assertEquals("user", user.getPassword());
        assertEquals("user1", user1.getPassword());

        userService.create(user);
        userService.create(user1);
    }

    @Test
    public void test2ResearchAllItem(){

        userList = userService.getAllItem();
        assertEquals(2, userList.size());

        user = userList.get(0);
        assertEquals("user@example.com", user.getEmail());
        assertEquals("user", user.getPassword());

        user1 = userList.get(1);
        assertEquals("user1@example.com", user1.getEmail());
        assertEquals("user1", user1.getPassword());
    }

    @Test
    public void test3ResearchByPrimaryKey(){

        userList = userService.getAllItem();
        user = userList.get(0);

        user2 = userService.getItemByPrimaryKey(user.getId());
        assertTrue(user.equals(user2));
    }

    @Test
    public void test4ResearchByField(){

        userList = userService.getAllItem();
        user = userList.get(0);

        userList = userService.research("email", "user@example.com");
        assertEquals(1, userList.size());

        user2 = userList.get(0);
        assertTrue(user.equals(user2));

        userList = userService.research("password", "r");
        assertEquals(2, userList.size());
    }

    @Test
    public void test5UpdateAllField(){

        userList = userService.getAllItem();
        user = userList.get(0);

        user.setEmail("pippo@example.com");
        user.setPassword("pippo");

        userService.update(user);

        userList = userService.getAllItem();
        user1 = userList.get(0);

        assertTrue(user1.getEmail().equals("pippo@example.com"));
        assertTrue(user1.getPassword().equals("pippo"));
    }

    @Test
    public void test6UserFollowUser(){

        userList = userService.getAllItem();
        user1 = userList.get(0);
        user2 = userList.get(1);

        user1.addFollower(user2);
        userService.update(user1);

        userList = userService.getAllItem();
        user1 = userList.get(0);
        user2 = userList.get(1);

        assertTrue(user2.getFollowings().contains(user1));
        assertTrue(user1.getFollowers().contains(user2));
    }

    @Test
    public void test7Delete(){

        List<User> users = userService.getAllItem();
        user = users.get(0);
        user1 = users.get(1);

        userService.delete(user.getId());
        userService.delete(user1.getId());

        assertTrue(userService.getAllItem().isEmpty());
    }

    @AfterClass
    public static void clearDatabase(){
        drop();
    }

    private static void drop(){
        final EntityManager entityManager = Persistence.createEntityManagerFactory("assignment3").createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.createNativeQuery("DELETE FROM user WHERE 1").executeUpdate();
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}